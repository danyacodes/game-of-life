import {
  init,
  runWorkers,
  updateCell,
  redraw,
  stopWorkers,
  clear,
} from "./threaded-gol.js";

const canvas = document.querySelector("canvas");
const ctx = canvas.getContext("2d");
const iterationCounter = document.getElementById("iteration");
const timeCounter = document.getElementById("time");

let width = 50;
let height = 50;
let iterations = 0;
const threads = 5;

canvas.width = width;
canvas.height = height;
init(threads, width, height);
redraw(ctx);

function onTick(dt) {
  iterationCounter.innerHTML = ++iterations;
  timeCounter.innerHTML = dt.toFixed(2);
  redraw(ctx);
}

document.getElementById("fillRandomButton").addEventListener("click", () => {
  stopWorkers();
  init(threads, width, height);
  for (let x = 0; x < width; x++) {
    for (let y = 0; y < height; y++) {
      const cell = Math.random() < 0.5 ? 0 : 1;
      updateCell(x, y, cell);
    }
  }
  redraw(ctx);

  iterations = 0;
  iterationCounter.innerHTML = 0;
});

const runButton = document.getElementById("runButton");
runButton.addEventListener("click", () => {
  runWorkers(onTick);
});

const inputWidth = document.getElementById("inputWidth");
const inputHeight = document.getElementById("inputHeight");

inputWidth.value = width;
inputHeight.value = height;

const setSizeButton = document.getElementById("setSizeButton");
setSizeButton.addEventListener("click", () => {
  stopWorkers();

  width = parseInt(inputWidth.value);
  height = parseInt(inputHeight.value);
  canvas.width = width;
  canvas.height = height;

  init(threads, width, height);
  redraw(ctx);
});

const clearButton = document.getElementById("clearButton");
clearButton.addEventListener("click", () => {
  stopWorkers();
  init(threads, width, height);
  redraw(ctx);

  iterations = 0;
  iterationCounter.innerHTML = 0;
});
