import * as constants from "./constants.js";

const { ALIVE_CELL, DEAD_CELL } = constants;

let sharedMemory;
let imageData;
let cells;
let sharedImageBuffer;
let sharedImageBuffer8;
let width;
let height;
let threads;

let running = false;
let calcWorkers = [];
let coordWorker;

export function init(_threads, _width, _height) {
  width = _width;
  height = _height;
  threads = _threads;

  const imageOffset = 2 * width * height;
  const syncOffset = imageOffset + 4 * width * height;

  sharedMemory = new SharedArrayBuffer(syncOffset + threads * 4);
  imageData = new ImageData(width, height);
  cells = new Uint8Array(sharedMemory, 0, imageOffset);
  sharedImageBuffer = new Uint32Array(sharedMemory, imageOffset);
  sharedImageBuffer8 = new Uint8ClampedArray(
    sharedMemory,
    imageOffset,
    4 * width * height
  );

  clear();
}

export function clear() {
  for (let x = 0; x < width; x++) {
    for (let y = 0; y < height; y++) {
      updateCell(x, y, 0);
    }
  }
}

export function updateCell(x, y, value) {
  sharedImageBuffer[width * y + x] = value === 1 ? ALIVE_CELL : DEAD_CELL;
  cells[width * y + x] = value;
}

export function redraw(ctx) {
  imageData.data.set(sharedImageBuffer8);
  ctx.putImageData(imageData, 0, 0);
}

export function runWorkers(onTick) {
  if (running) return;

  const chunkSize = height / threads;
  for (let i = 0; i < threads; i++) {
    const worker = new Worker("workers/calc-worker.js", {
      name: `calc-worker-${i}`,
      type: "module",
    });
    worker.postMessage({
      range: [0, chunkSize * i, width, chunkSize * (i + 1)],
      sharedMemory,
      i,
      width,
      height,
    });
    calcWorkers.push(worker);
  }

  coordWorker = new Worker("workers/coord-worker.js", {
    name: "coordination-worker",
    type: "module",
  });
  coordWorker.postMessage({
    coord: true,
    sharedMemory,
    width,
    height,
    threads,
  });

  let startTime = performance.now();
  coordWorker.addEventListener("message", () => {
    onTick(performance.now() - startTime);
    startTime = performance.now();
    window.requestAnimationFrame(() => coordWorker.postMessage({}));
  });

  running = true;
}

export function pause() {
  running = false;
}

export function unpause() {
  running = true;
}

export function stopWorkers() {
  if (!running) return;
  for (const worker of calcWorkers) {
    worker.terminate();
  }
  coordWorker.terminate();
  calcWorkers = [];
  running = false;
}
