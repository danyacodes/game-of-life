export default class Grid {
  constructor(width, height, buffer) {
    const size = width * height;
    this.width = width;
    this.height = height;
    this.buffer = buffer;
    this.cells = new Uint8Array(this.buffer, 0, size);
    this.nextCells = new Uint8Array(this.buffer, size, size);
  }

  getCell(x, y) {
    x = x < 0 ? this.width - 1 : x > this.width - 1 ? 0 : x;
    y = y < 0 ? this.height - 1 : y > this.height - 1 ? 0 : y;
    return this.cells[this.width * y + x];
  }

  static NEIGHBORS = [
    [-1, -1],
    [-1, 0],
    [-1, 1],
    [0, -1],
    [0, 1],
    [1, -1],
    [1, 0],
    [1, 1],
  ];

  iterate(minX, minY, maxX, maxY) {
    for (let x = minX; x < maxX; x++) {
      for (let y = minY; y < maxY; y++) {
        const cell = this.cells[this.width * y + x];
        let alive = 0;
        for (const [i, j] of Grid.NEIGHBORS) {
          alive += this.getCell(x + i, y + j);
        }
        const newCell = alive === 3 || (cell && alive === 2) ? 1 : 0;
        this.nextCells[this.width * y + x] = newCell;
      }
    }
    const cells = this.nextCells;
    this.nextCells = this.cells;
    this.cells = cells;
  }
}
