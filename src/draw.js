import { updateCell, redraw } from "./threaded-gol.js";

const canvas = document.querySelector("canvas");
const ctx = canvas.getContext("2d");
let drawing = false;

function toggleCell(x, y, value = 1) {
  updateCell(x, y, value);
  redraw(ctx);
}

function getCursorPosition(canvas, event) {
  const rect = canvas.getBoundingClientRect();
  const cellWidth = (rect.right - rect.left) / canvas.width;
  const cellHeight = (rect.bottom - rect.top) / canvas.height;
  const x = Math.floor((event.clientX - rect.left) / cellWidth);
  const y = Math.floor((event.clientY - rect.top) / cellHeight);
  return [x, y];
}

canvas.addEventListener("mousedown", function (e) {
  drawing = true;
  const [x, y] = getCursorPosition(canvas, e);
  toggleCell(x, y, e.buttons === 1 ? 1 : 0);
});

canvas.addEventListener("mousemove", function (e) {
  if (drawing) {
    const [x, y] = getCursorPosition(canvas, e);
    toggleCell(x, y, e.buttons === 1 ? 1 : 0);
  }
});

canvas.addEventListener("mouseup", function (e) {
  drawing = false;
});
