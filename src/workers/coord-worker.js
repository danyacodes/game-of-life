import * as constants from "../constants.js";

const { ALIVE_CELL, DEAD_CELL } = constants;

self.addEventListener("message", initListener);

let sharedMemory;
let sync;
let sharedImageBuffer;
let cells;
let nextCells;
let threads;
let width;
let height;
let imageOffset;
let syncOffset;

function initListener(msg) {
  const opts = msg.data;
  sharedMemory = opts.sharedMemory;

  threads = opts.threads;
  width = opts.width;
  height = opts.height;
  imageOffset = 2 * width * height;
  syncOffset = imageOffset + 4 * width * height;

  sync = new Int32Array(sharedMemory, syncOffset);
  self.removeEventListener("message", initListener);
  self.addEventListener("message", runCoord);
  cells = new Uint8Array(sharedMemory);
  nextCells = new Uint8Array(sharedMemory, width * height);
  sharedImageBuffer = new Uint32Array(sharedMemory, imageOffset);

  runCoord();
}

function runCoord() {
  for (let i = 0; i < threads; i++) {
    Atomics.store(sync, i, 1);
    Atomics.notify(sync, i);
  }
  for (let i = 0; i < threads; i++) {
    Atomics.wait(sync, i, 1);
  }

  const oldCells = cells;
  cells = nextCells;
  nextCells = oldCells;

  for (let x = 0; x < width; x++) {
    for (let y = 0; y < height; y++) {
      sharedImageBuffer[width * y + x] = cells[width * y + x]
        ? ALIVE_CELL
        : DEAD_CELL;
    }
  }

  self.postMessage({});
}
