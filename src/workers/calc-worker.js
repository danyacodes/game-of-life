import Grid from "../grid.js";

let sharedMemory;
let sync;
let width;
let height;
let imageOffset;
let syncOffset;

self.addEventListener("message", initListener);

function initListener(msg) {
  const opts = msg.data;
  sharedMemory = opts.sharedMemory;

  width = opts.width;
  height = opts.height;
  imageOffset = 2 * width * height;
  syncOffset = imageOffset + 4 * width * height;
  sync = new Int32Array(sharedMemory, syncOffset);
  self.removeEventListener("message", initListener);

  runWorker(opts);
}

function runWorker({ range, i }) {
  const grid = new Grid(width, height, sharedMemory);
  while (true) {
    Atomics.wait(sync, i, 0);
    grid.iterate(...range);
    Atomics.store(sync, i, 0);
    Atomics.notify(sync, i);
  }
}
